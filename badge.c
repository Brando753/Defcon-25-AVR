/*
 * This file inculdes Board Specific Functions and variables
 * as well as defines for components specific to this badge.
 */

#include "badge.h"


/*
 * This inlined function will set the RED,
 * GREEN, and BLUE led pins as outputs as well
 * as set the input button as an input pin and
 * setup the interrupts for a push event.
 * RUN THIS AS YOUR FIRST LINE OF CODE
 */
void setup_board(void)
{
     enable_leds();
     SET_INPUT(M_BUTTON_DIRECTION, M_BUTTON);
     PCICR |= (1<<PCIE0); /* The falling edge of INT0 generates an interupt */
     PCMSK0 |= (1<<0); /* Extenral Pin (INT0) interrupt Enable */

     /* Set input and pull-up unconnected pins */
     SET_INPUT(DDRB, PB6);
     SET_INPUT(DDRB, PB7);
     SET_INPUT(DDRC, PC5);
     SET_INPUT(DDRD, PD0);
     SET_INPUT(DDRD, PD1);
     SET_INPUT(DDRD, PD2);
     SET_INPUT(DDRD, PD4);
     SET_INPUT(DDRD, PD7);
     SET_HIGH(PORTB, PB6);
     SET_HIGH(PORTB, PB7);
     SET_HIGH(PORTC, PC5);
     SET_HIGH(PORTD, PD0);
     SET_HIGH(PORTD, PD1);
     SET_HIGH(PORTD, PD2);
     SET_HIGH(PORTD, PD4);
     SET_HIGH(PORTD, PD7);


     enable_pwm_timers();

     sei();

     return;
}

void enable_leds(void) {
  SET_OUTPUT(RGB_1_DIRECTION, RGB_1_RED);
  SET_OUTPUT(RGB_1_DIRECTION, RGB_1_GREEN);
  SET_OUTPUT(RGB_1_DIRECTION, RGB_1_BLUE);
  SET_OUTPUT(RGB_2_DIRECTION, RGB_2_RED);
  SET_OUTPUT(RGB_2_DIRECTION, RGB_2_GREEN);
  SET_OUTPUT(RGB_2_DIRECTION, RGB_2_BLUE);

  SET_OUTPUT(LED_DIRECTION, LED_1);
  SET_OUTPUT(LED_DIRECTION, LED_2);
  SET_OUTPUT(LED_DIRECTION, LED_3);
  SET_OUTPUT(LED_DIRECTION, LED_4);
  SET_OUTPUT(LED_DIRECTION, LED_5);
  turn_off_leds();
  return;
}

void turn_off_leds(void) {
  SET_LOW(LED_DIRECTION, LED_1);
  SET_LOW(LED_DIRECTION, LED_2);
  SET_LOW(LED_DIRECTION, LED_3);
  SET_LOW(LED_DIRECTION, LED_4);
  SET_LOW(LED_DIRECTION, LED_5);
  return;
}

void enable_pwm_timers(void) {
  /* Enable Timer 0 (RGB_2 Blue and Green) */
  TCCR0A |= (1<<COM0A1) | (1<<COM0B1) | (1<<WGM00);
  TCCR0B |= (1<<CS00);

  /* Enable Timer 1 (RGB_1 Red and Green) */
  TCCR1A |= (1<<COM1A1) | (1<<COM1B1) | (1<<WGM10);
  TCCR1B |= (1<<CS10);

  /* Enable Timer 2 (RGB_2 Red and RGB_1 Blue) */
  TCCR2A |= (1<<COM2A1) | (1<<COM2B1) | (1<<WGM00);
  TCCR2B |= (1<<CS20);

  /* Set all PWM values to Off */
  RED_1 = 0x00;
  GREEN_1 = 0x00;
  BLUE_1 = 0x00;
  RED_2 = 0x00;
  GREEN_2 = 0x00;
  BLUE_2 = 0x00;

  return;
}

void disable_pwm_timers(void) {
  /* Disable Timer 0 */
  TCCR0A &= ~((1<<COM0A1) | (1<<COM0B1));
  TCCR0B &= ~(1<<CS00);

  /* Disable Timer 1 */
  TCCR1A &= ~((1<<COM1A1) | (1<<COM1B1));
  TCCR1B &= ~(1<<CS10);
  /* Disable Timer 2 */
  TCCR2A &= ~((1<<COM2A1) | (1<<COM2B1));
  TCCR2B &= ~(1<<CS20);

  return;
}
