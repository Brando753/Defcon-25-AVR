#include "badge.h"

#define PROGRAM_MODES 4
volatile int mode = 0; /* Program Mode */

/* Reset RGB's to off for next mode */
void clear_rgb(void) {
  RED_1 = 0x00;
  RED_2 = 0x00;
  GREEN_1 = 0x00;
  GREEN_2 = 0x00;
  BLUE_1 = 0x00;
  BLUE_2 = 0x00;
  return;
}

/*
* Hardware interrupt called when main button
* is pressed,  (Goes twice per push (once on push
* and one on release)
*/
ISR(PCINT0_vect)
{
  static volatile bool rising_edge = false; /* Variable to track if push or release */
  if(rising_edge == false){
    rising_edge = true; /* DO NOT REMOVE */
    /* Code for button pushed goes here */
    mode = (mode + 1) % PROGRAM_MODES;
    clear_rgb();
  }
  else
  {
    rising_edge = false; /* DO NOT REMOVE */
    /* Code for button released goes here */
  }
}

int main() {
  setup_board();
  int leds[] = {LED_1, LED_2, LED_3, LED_4, LED_5};
  int i = 0;
  int j;
  int intensity=0;
  int modifier = 1;

  for(;;){
    if (i % 5 == 0) {
      SET_LOW(LED_DIRECTION,leds[4]);
      SET_HIGH(LED_DIRECTION,leds[0]);
      i = 1;
    }
    else {
      SET_LOW(LED_DIRECTION, leds[i-1]);
      SET_HIGH(LED_DIRECTION, leds[i]);
      i++;
    }

    for(j = 0; j<20; j++)
    {
      intensity += modifier;
      if(intensity == 0xFF){
        modifier = -1;
      }
      else if(intensity == 0x00){
        modifier = 1;
      }

      switch(mode) {
        case 0: /* RED LED */
        RED_1 = intensity;
        RED_2 = intensity;
        break;
        case 1: /* BLUE LED */
        BLUE_1 = intensity;
        BLUE_2 = intensity;
        break;
        case 2: /* GREEN LED */
        GREEN_1 = intensity;
        GREEN_2 = intensity;
        break;
        case 3: /* GO TO SLEEP */
        disable_pwm_timers();
        turn_off_leds();
        go_to_sleep();
        enable_pwm_timers();
        break;
      }
      _delay_ms(10);
    }

  }
  return 0; /* Comply with C standard */
}
