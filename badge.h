#ifndef BADGE_LLAMA
#define BADGE_LLAMA
/*
 * This file inculdes Board Specific Functions and variables
 * as well as defines for components specific to this badge.
 */

#define F_CPU 1000000L
#include <avr/io.h> /* Various AVR related Macros for I/O */
#include <avr/interrupt.h> /* Gives access to interrupt on push button */
#include <avr/sleep.h> /* Puts the AVR micro to sleep so it does not waste power when not in use */
#include <stdint.h> /* Defines uint data types */
#include "utils.h"

/* Define chip specific macros */

/* Define Defcon 25 badge specific components */
#define M_BUTTON PB0
#define M_BUTTON_PORT PORTB
#define M_BUTTON_DIRECTION DDRB

#define RGB_1_RED PB1
#define RGB_1_GREEN PB2
#define RGB_1_BLUE PB3
#define RGB_1_PORT PORTB
#define RGB_1_DIRECTION DDRB

#define RGB_2_RED PD3
#define RGB_2_GREEN PD5
#define RGB_2_BLUE PD6
#define RGB_2_PORT PORTD
#define RGB_2_DIRECTION DDRD

#define RED_1 OCR1AL
#define GREEN_1 OCR1BL
#define BLUE_1 OCR2A

#define RED_2 OCR2B
#define BLUE_2 OCR0A
#define GREEN_2 OCR0B

#define LED_1 PC0
#define LED_2 PC1
#define LED_3 PC2
#define LED_4 PC3
#define LED_5 PC4
#define LED_PORT PORTC
#define LED_DIRECTION DDRC


void enable_pwm_timers(void);
void disable_pwm_timers(void);
void setup_board(void);
void enable_leds(void);
void turn_off_leds(void);

#endif
