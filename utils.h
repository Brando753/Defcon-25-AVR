#ifndef AVR_UTILS
#define AVR_UTILS
/*
 * This file inculdes Utility Specific Functions and variables
 */

#include "badge.h"
#include <avr/io.h> /* Various AVR related Macros for I/O */
#include <avr/interrupt.h> /* Gives access to interrupt on push button */
#include <avr/sleep.h> /* Puts the AVR micro to sleep so it does not waste power when not in use */
#include <util/delay.h> /* Gives functions for delaying and CPU macros */
#include <stdint.h> /* Defines uint data types */
#include <stdbool.h>

/* Define reusable macro for bit maipulation and pin management */
#define SET_OUTPUT(port_direction, pin) port_direction |= (1<<pin)
#define SET_INPUT(port_direction, pin) port_direction &= ~(1<<pin)
#define SET_LOW(port, pin) port &= ~(1<<pin)
#define SET_HIGH(port, pin) port |= (1<<pin)
#define GET_VALUE(port, pin) (PINB & (1<<pin))
#define TOGGLE_VALUE(port, pin) port ^= (1<<pin)

/*
 * Puts the processor to sleep into low power
 * mode, interupt required to wake the processor
 * back up. Would start where it was lost.
 */
void go_to_sleep(void);
#endif
