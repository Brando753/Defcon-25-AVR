/*
 * This file inculdes Board Specific Functions and variables
 * as well as defines for components specific to this badge.
 */

#include "utils.h"

/*
 * Shut the processor off, requires interrupt to star the CPU back up
 */
void go_to_sleep(void)
{
     cli();
     set_sleep_mode(SLEEP_MODE_PWR_DOWN);
     sleep_enable();
     sei();
     sleep_cpu();
     sleep_disable(); /* After waking up clear sleep bits */
     return;
}
