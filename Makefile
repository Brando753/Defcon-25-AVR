CC=avr-gcc
CFLAGS=-I. -g -Os -Wall -Werror -pedantic -Wl,-static -ffunction-sections -Wl,--gc-sections
LDFLAGS=
OBJ=avr-objcopy
SIZE=avr-size

MICRO=atmega48p
P_MICRO=m48
PROGRAMMER=usbasp
OUTFORMAT=ihex
OUTEXT=hex

# Add additional C files to the sources below
SOURCES=main.c utils.c badge.c
OBJECTS=$(SOURCES:.c=.o)
OUTPUT=badge


all: $(SOURCES) $(OUTPUT).$(OUTEXT)

.PHONY: clean flash erase

$(OUTPUT).$(OUTEXT): $(OBJECTS)
	$(CC) $(CFLAGS) $(LDFLAGS) -mmcu=$(MICRO) $(OBJECTS) -o $(OUTPUT).elf
	$(OBJ) -j .text -j .data -O $(OUTFORMAT) $(OUTPUT).elf $@
	$(SIZE) --mcu=$(MICRO) -C $(OUTPUT).elf

.c.o:
	$(CC) $(CFLAGS) -mmcu=$(MICRO) -c $< -o $@

flash: $(OUTPUT).$(OUTEXT)
	avrdude -p $(P_MICRO) -B 3 -c $(PROGRAMMER) -U flash:w:$(OUTPUT).$(OUTEXT)

erase:
	avrdude -p $(P_MICRO) -B 3 -c $(PROGRAMMER) -e

clean:
	rm -f $(OBJECTS)
	rm -f $(OUTPUT).elf
	rm -f $(OUTPUT).$(OUTEXT)
